#!/bin/bash

current_brightness=$(brightnessctl g | cut -d '(' -f 2 | cut -d ')' -f 1)

if [ "$1" == "+" ]; then
    if [ $current_brightness -lt 10 ]; then
        brightnessctl set +1
    elif [ $current_brightness -ge 10 ] && [ $current_brightness -lt 100 ]; then
        brightnessctl set +7
    else
        brightnessctl set +50
    fi
elif [ "$1" == "-" ]; then
    if [ $current_brightness -le 10 ]; then
        brightnessctl set 1-
    elif [ $current_brightness -gt 10 ] && [ $current_brightness -le 130 ]; then
        brightnessctl set 7-
    else
        brightnessctl set 50-
    fi

    # Make sure brightness doesn't go below 2
    new_brightness=$(brightnessctl g | cut -d '(' -f 2 | cut -d ')' -f 1)
    if [ $new_brightness -lt 2 ]; then
        brightnessctl set 2
    fi
fi

