#!/bin/bash

# Define your display identifier, e.g., DVI-I-0
DISPLAY_IDENTIFIER="DVI-I-0"

# Get the current brightness using xrandr
brightness=$(xrandr --verbose | grep -m 1 'Brightness' | cut -f2 -d ' ')

# Compare brightness values as floating-point numbers
if (( $(echo "$brightness <= 0.2" | bc -l) )); then
    icon="󰃞"  # Dimmest
elif (( $(echo "$brightness > 0.2 && $brightness <= 0.4" | bc -l) )); then
    icon="󰃝"  # Dim
elif (( $(echo "$brightness > 0.4 && $brightness <= 0.6" | bc -l) )); then
    icon="󰃟"  # Moderate
elif (( $(echo "$brightness > 0.6 && $brightness <= 0.8" | bc -l) )); then
    icon="󰃟"  # Bright
elif (( $(echo "$brightness > 0.8 && $brightness <= 1" | bc -l) )); then
    icon="󰃠"  # Brighter
else
    icon="󰃠"  # Brightest
fi

echo "$icon"
