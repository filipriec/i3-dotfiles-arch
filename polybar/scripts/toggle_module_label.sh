#!/usr/bin/env bash

# The argument $1 should be the name of the module you want to toggle
TOGGLE_FILE="$HOME/.toggle_${1}_label"

if [ -f "$TOGGLE_FILE" ]; then
  rm "$TOGGLE_FILE"
else
  touch "$TOGGLE_FILE"
fi

polybar-msg hook $1 1
