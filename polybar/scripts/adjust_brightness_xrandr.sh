#!/bin/bash

# Set your display identifier
DISPLAY_IDENTIFIER="DVI-I-0"

# Get the current brightness
current_brightness=$(xrandr --verbose | grep -m 1 -i brightness | cut -f2 -d ' ')

# Determine the step value for brightness changes based on current brightness
if (( $(echo "$current_brightness < 0.2" | bc -l) )); then
    step=0.0125  # Finer step for the dimmest settings
elif (( $(echo "$current_brightness < 0.5" | bc -l) )); then
    step=0.0375    # Smaller step for dim settings
else
    step=0.075     # Larger step for brighter settings
fi

# Increase or decrease brightness based on the first argument
if [ "$1" == "+" ]; then
    new_brightness=$(echo "$current_brightness + $step" | bc)
elif [ "$1" == "-" ]; then
    new_brightness=$(echo "$current_brightness - $step" | bc)
fi

# Format the brightness to avoid locale issues with decimal separator
new_brightness=$(LC_ALL=C printf "%.4f" $new_brightness)

# Clamp the new brightness values to be within 0.1 and 1
if (( $(echo "$new_brightness < 0.1" | bc -l) )); then
    new_brightness=0.1
elif (( $(echo "$new_brightness > 1" | bc -l) )); then
    new_brightness=1
fi

# Apply the new brightness value
xrandr --output $DISPLAY_IDENTIFIER --brightness $new_brightness
