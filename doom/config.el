;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
;; (setq user-full-name "John Doe"
;;       user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-symbol-font' -- for symbols
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
;;
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory ".backups"))))
(setq display-line-numbers-type 'relative)
(add-hook 'evil-normal-state-entry-hook #'evil-ex-nohighlight)

  (defvar my/farmeris-active nil "Whether the custom farmeris command is active.")

  (defun my/toggle-farmeris ()
    "Toggle Treemacs and open vterm in a specific directory."
    (interactive)
    (if my/farmeris-active
        (progn
          ;; Deactivate farmeris
          (setq my/farmeris-active nil)
          (message "Farmeris deactivated"))
      (progn
        ;; Activate farmeris
        (setq my/farmeris-active t)
        (let ((default-directory "~/Documents/farmeris/farmeris"))
          (treemacs-add-and-display-current-project)
          (+vterm/here 1))
        (message "Farmeris activated"))))

  (map! :leader
        :desc "Toggle Farmeris"
        "farmeris" #'my/toggle-farmeris)

(after! vertico
  (setq completion-styles
        '(substring orderless))
  )

(defun find-django-project-root ()
  "Find the root of the Django project by looking for the manage.py file."
  (locate-dominating-file (buffer-file-name) "manage.py"))

(defun find-django-template-dirs (project-root)
  "Recursively find all 'templates' directories within the Django project."
  (let ((dirs (directory-files-recursively project-root "templates" t)))
    ;; Filter directories to include only those that are named 'templates'
    (seq-filter (lambda (dir) (and (file-directory-p dir)
                                   (string= (file-name-nondirectory dir) "templates")))
                dirs)))

;;; Function to find Django static directories
(defun find-django-static-dirs (project-root)
  "Recursively find all 'static' directories within the Django project."
  (let ((dirs (directory-files-recursively project-root "static" t)))
    ;; Filter directories to include only those named 'static'
    (seq-filter (lambda (dir) (and (file-directory-p dir)
                                   (string= (file-name-nondirectory dir) "static")))
                dirs)))

(defun open-django-template-new ()
  "Open a Django template or static file from a reference in a template or Python code."
  (interactive)
  ;; Regex to match file paths in Django template include tags or Python code
  (let ((regex "\\({% static ['\"]\\)?\\([^'\"]+\\(\\.html\\|\\.css\\)\\)['\"]? %}?\\|\\(\"\\|'\\)\\([^'\"]+\\(\\.html\\|\\.css\\)\\)\\(\"\\|'\\)")
        project-root static-dirs file-path)
    (setq project-root (find-django-project-root))
    (setq static-dirs (append (find-django-template-dirs project-root)
                              (find-django-static-dirs project-root)))
    (save-excursion
      (beginning-of-line)
      (when (re-search-forward regex (line-end-position) t)
        (setq file-path (or (match-string 2) (match-string 5)))))
    (when file-path
      (let ((full-path (cl-find-if 'file-exists-p
                                   (mapcar (lambda (dir) (concat dir "/" file-path)) static-dirs))))
       (if full-path
            (find-file-other-window full-path)
          (message "Template not found: %s" file-path))))))

(map! :map html-mode-map
      "C-c o" #'open-django-template-new)

(with-eval-after-load 'html-mode
  (define-key html-mode-map (kbd "C-c o") #'open-django-template-new))

(with-eval-after-load 'web-mode
  (define-key web-mode-map (kbd "C-c o") #'open-django-template-new))

(with-eval-after-load 'python
  (define-key python-mode-map (kbd "C-c o") #'open-django-template-new))

(defun open-django-template-same ()
  "Open a Django template or static file from a reference in a template or Python code."
  (interactive)
  ;; Regex to match file paths in Django template include tags or Python code
  (let ((regex "\\({% static ['\"]\\)?\\([^'\"]+\\(\\.html\\|\\.css\\)\\)['\"]? %}?\\|\\(\"\\|'\\)\\([^'\"]+\\(\\.html\\|\\.css\\)\\)\\(\"\\|'\\)")
        project-root static-dirs file-path)
    (setq project-root (find-django-project-root))
    (setq static-dirs (append (find-django-template-dirs project-root)
                              (find-django-static-dirs project-root)))
    (save-excursion
      (beginning-of-line)
      (when (re-search-forward regex (line-end-position) t)
        (setq file-path (or (match-string 2) (match-string 5)))))
    (when file-path
      (let ((full-path (cl-find-if 'file-exists-p
                                   (mapcar (lambda (dir) (concat dir "/" file-path)) static-dirs))))
        (if full-path
            (switch-to-buffer (find-file-noselect full-path))
          (message "File not found: %s" file-path))))))
;; Bind the function to C-c C-o in both HTML and Python modes
(map! :map html-mode-map
      "C-c C-o" #'open-django-template-same)

(with-eval-after-load 'html-mode
  (define-key html-mode-map (kbd "C-c C-o") #'open-django-template-same))

(with-eval-after-load 'web-mode
  (define-key web-mode-map (kbd "C-c C-o") #'open-django-template-same))

(with-eval-after-load 'python
  (define-key python-mode-map (kbd "C-c C-o") #'open-django-template-same))

(use-package python-black
  :demand t
  :after python)

(use-package flycheck
  :init (global-flycheck-mode))

(defun insert-line-above-normal-mode ()
  "Insert a new line above the current line in normal mode."
  (interactive)
  (previous-line)
  (end-of-line)
  (newline-and-indent)
  (evil-force-normal-state))

(map! :n "M-O" #'insert-line-above-normal-mode)

(defun insert-line-below-normal-mode ()
  "Insert a new line below the current line in normal mode."
  (interactive)
  (end-of-line)
  (newline-and-indent)
  (evil-force-normal-state))

(map! :n "M-o" #'insert-line-below-normal-mode)
