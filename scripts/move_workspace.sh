#!/bin/bash

TARGET="$1"

# Exit if no target is specified
if [ -z "$TARGET" ]; then
    echo "Usage: $0 <target_workspace_number>"
    exit 1
fi

# Get the current workspace number and all workspace numbers
CURRENT=$(i3-msg -t get_workspaces | jq -r '.[] | select(.focused).num')
WORKSPACES=($(i3-msg -t get_workspaces | jq -r '.[] | .num' | sort -n))

# Find the index of the current workspace and target workspace
CURRENT_INDEX=$(printf "%s\n" "${WORKSPACES[@]}" | grep -n "^$CURRENT$" | cut -d: -f1)
TARGET_INDEX=$(printf "%s\n" "${WORKSPACES[@]}" | grep -n "^$TARGET$" | cut -d: -f1)

# Calculate the direction of movement
if [ -z "$TARGET_INDEX" ]; then
    # The target workspace doesn't exist, we can move directly
    i3-msg "rename workspace $CURRENT to $TARGET"
else
    # The target workspace exists, determine the direction of shift
    if [ "$CURRENT_INDEX" -lt "$TARGET_INDEX" ]; then
        # Moving to a higher number
        for (( i=CURRENT_INDEX; i<TARGET_INDEX; i++ )); do
            i3-msg "rename workspace ${WORKSPACES[$i]} to TEMP"
            i3-msg "rename workspace ${WORKSPACES[$i+1]} to ${WORKSPACES[$i]}"
            i3-msg "rename workspace TEMP to ${WORKSPACES[$i+1]}"
        done
    elif [ "$CURRENT_INDEX" -gt "$TARGET_INDEX" ]; then
        # Moving to a lower number
        for (( i=CURRENT_INDEX-1; i>=TARGET_INDEX; i-- )); do
            i3-msg "rename workspace ${WORKSPACES[$i]} to TEMP"
            i3-msg "rename workspace ${WORKSPACES[$i-1]} to ${WORKSPACES[$i]}"
            i3-msg "rename workspace TEMP to ${WORKSPACES[$i-1]}"
        done
    fi
    # Finally, move the current workspace to the target position
    i3-msg "rename workspace $CURRENT to $TARGET"
fi

# Focus on the new workspace
i3-msg "workspace number $TARGET"

